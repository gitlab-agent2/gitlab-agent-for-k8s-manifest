# Continuous Deployment with Agent for Kubernetes and GitLab pipeline

blog: https://fabianlee.org/2023/10/20/gitlab-continuous-deployment-with-agent-for-kubernetes-and-gitlab-pipeline/

Shows how to deploy the GitLab Agent for Kubernetes into a GitLab project and target Kubernetes cluster.  

This will grant the pipeline kubectl level access to the Kubernetes cluster, and therefore a way to deploy workloads to a live environment.

# Pulling image from GitHub Container Registry

```
docker pull registry.gitlab.com/gitlab-agent2/gitlab-agent-for-k8s-manifest:latest
```

# Creating tag that invokes Github Action

```
newtag=v1.0.1
git commit -a -m "changes for new tag $newtag" && git push -o ci.skip
git tag $newtag && git push origin $newtag
```

# Deleting tag

```
# delete local tag, then remote
todel=v1.0.1
git tag -d $todel && git push -d origin $todel
```

